/* global use, db */
// MongoDB Playground
// Use Ctrl+Space inside a snippet or a string literal to trigger completions.

// The current database to use.
use('balance');

// Search for documents in the current collection.
db.getCollection('groups')
  .find(
    {
      id: "aa760d86-172b-48a0-9918-c5af63bc46f5",
    }
  );
