/* global use, db */
// MongoDB Playground
// Use Ctrl+Space inside a snippet or a string literal to trigger completions.

// The current database to use.
use('balance');

// Search for documents in the current collection.
db.getCollection('tasks')
  .find(
    {
      id: 'a1eee2db-ab9a-44fa-9c28-a6baa814f564',
    },
  );
