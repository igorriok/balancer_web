var db = connect("mongodb://balancer:balancethis@localhost:27017/admin");

db = db.getSiblingDB('balance'); // we can not use "use" statement here to switch db

db.createUser(
    {
        user: "balancer",
        pwd: "balancethis",
        roles: [ { role: "readWrite", db: "balance"} ],
        passwordDigestor: "server",
    }
)

db.user.insertOne({name: "test"})