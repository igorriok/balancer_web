import dbConnection from "./dbConnection.js";
import { Done } from "./entities/Done";
import { Task } from "./entities/Task";


/**
 * Extract date string from task
 * @param {Task} task task object from to get date string
 * @return {string} Date string
 */
export default async function getDateString(task: Task): Promise<string> {

	let dateString: string = "";
	const currentDateTime = new Date();

	if (task.recurring) {

		if (task.reminderDate) {

			dateString = task.reminderDate;

			if (task.reminderTime) {
				// if we have date, add time
				dateString = dateString + "T" + task.reminderTime + task.offset;
			} else {
				// else add default time
				dateString = dateString + "T08:00:00" + task.offset;
			}

		} else if (task.reminderTime) {

			const dones: Done[] = await dbConnection.getDonesByTask(task.id);

			const lastDone: Done = dones.sort((a: Done, b: Done) => {
				if (!a.date || !b.date) return 1;
				const aDate = new Date(a.date);
				const bDate = new Date(b.date);
				return aDate < bDate ? 1 : -1;
			})[0];

			const lastDoneDateString = lastDone?.date || task.addedDate;

			console.log(`LastDoneDateString for task: ${task.name} `, lastDoneDateString);

			const lastDoneDate: Date = new Date(lastDoneDateString);
			console.log("lastDoneDate: ", lastDoneDate);
			let nextDate: Date = new Date(lastDoneDate.setDate(lastDoneDate.getDate() + (task.interval || 1)));

			while (nextDate <= currentDateTime) {
				nextDate = new Date(lastDoneDate.setDate(lastDoneDate.getDate() + (task.interval || 1)));
			}

			const nextDateString = nextDate.toISOString().split("T")[0];

			const taskDateTime: Date = new Date(
				nextDateString + "T" + task.reminderTime,
			);
			console.log(`Task ${task.name} dateTime `, taskDateTime.toISOString());

			dateString = taskDateTime.toISOString();

		} else {

			const dones: Done[] = await dbConnection.getDonesByTask(task.id);

			const lastDone: Done = dones.sort((a: Done, b: Done) => {
				if (!a.date || !b.date) return 1;
				const aDate = new Date(a.date);
				const bDate = new Date(b.date);
				return aDate < bDate ? 1 : -1;
			})[0];

			const lastDoneDateString: string = lastDone?.date || task.addedDate;

			console.log(`lastDoneDateString for task: ${task.name} is `, lastDoneDateString);

			if (typeof lastDoneDateString === "string") {

				const lastDoneDate: Date = new Date(lastDoneDateString);
				console.log(`lastDoneDate: for task ${task.name} is `, lastDoneDate);

				let nextDate: Date = new Date(lastDoneDate.setDate(lastDoneDate.getDate() + (task.interval || 1)));

				while (nextDate <= currentDateTime) {
					nextDate = new Date(lastDoneDate.setDate(lastDoneDate.getDate() + (task.interval || 1)));
				}
				console.log(`nextDate for task: ${task.name} is ${lastDoneDate}`);

				dateString = nextDate.toISOString();
			}
		}

	} else {
		if (task.reminderDate) {
			dateString = task.reminderDate;

			if (task.reminderTime) {
				// if we have date, add time
				dateString = dateString + "T" + task.reminderTime + task.offset;
			} else {
				// else add default time
				dateString = dateString + "T08:00:00" + task.offset;
			}
		} else {
			if (task.reminderTime) {
				dateString = currentDateTime.toISOString().split("T")[0];
				dateString = dateString + "T" + task.reminderTime + task.offset;
			}
		}
	}

	return dateString;
}
