export interface User {
	type: "user";
	email: string;
	displayName: string;
	photoURL: string;
	id: string;
	accessToken: string;
	roles: string[];
	groups?: string[];
	lastDateModified: string;
}
