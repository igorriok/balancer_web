import { Done } from "./Done"
import { Group } from "./Group"
import { Invitation } from "./Invitation"
import { Task } from "./Task"
import { Token } from "./Token"
import { User } from "./User"

export type Message = {
    type: string,
    data: Task | Done | User | Group | Token | Invitation,
}