import { Job } from "node-schedule";

export type Reminder = {
	taskId: string;
	job: Job;
}