export type Token = {
    email: string;
    id: string;
    lastDateModified: string;
}