export type Invitation = {
	email: string;
	groupId: string;
}
