export interface Group {
	id: string;
	name: string;
	author?: string;
	participants: string[];
	active: boolean;
	lastDateModified: string;
	type: string;
	invited?: string[];
}
