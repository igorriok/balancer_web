export type Score = {
  id: string;
  authorEmail: string;
  points: number;
  date: string;
  doneId: string;
  groupId: string;
  doneAuthorEmail: string;
  doneDate: string;
  taskName: string;
  active: boolean;
  lastDateModified: string;
  type: string;
};
