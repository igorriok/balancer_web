import winston, { transports, Logger } from "winston";
import "winston-daily-rotate-file";


const transport = new transports.DailyRotateFile({
	level: "info",
	filename: "./logs/balancer-%DATE%.log",
	datePattern: "YYYY-MM-DD",
	//zippedArchive: true,
	maxSize: "20m",
	maxFiles: "7d"
});

class LoggerSingleton {
	#logger: Logger;

	static #instance: LoggerSingleton;

	private constructor() {
		this.#logger = winston.createLogger({
			level: "info",
			format: winston.format.combine(
				winston.format.timestamp(),
				winston.format.json(),
			),
			//defaultMeta: { service: 'user-service' },
			transports: [
				new winston.transports.Console({
					format: winston.format.combine(
						winston.format.colorize(),
					)
				}),
				transport,
				// - Write all logs with importance level of `error` or less to `error.log`
				// - Write all logs with importance level of `info` or less to `combined.log`
				//new winston.transports.File({ filename: 'error.log', level: 'error' }),
				//new winston.transports.File({ filename: 'combined.log' }),
			],
		});
	}

	static getInstance() {
		if (!this.#instance) {
			this.#instance = new LoggerSingleton();
		}
		return this.#instance;
	}

	public get logger() {
		return this.#logger;
	}
}

const logger: Logger = LoggerSingleton.getInstance().logger;

export default logger;