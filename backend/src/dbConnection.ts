/* eslint-disable no-unused-vars */
import { Db, MongoClient, ObjectId, Document } from "mongodb";
import { Task } from "./entities/Task";
import { User } from "./entities/User";
import { Group } from "./entities/Group";
import { Done } from "./entities/Done";
import { Token } from "./entities/Token";
import logger from "./logger.js";
import { Score } from "./entities/Score";


// Replace the uri string with your connection string.
const uri = "mongodb://balancer:balancethis@ciupa.local:27017,ciupa.local:27018,ciupa.local:2701/?replicaSet=mongors";

const client = new MongoClient(uri);
let db: Db;

async function connect(): Promise<Db> {
	const database = client.db("balance");
	db = database;
	return database;
}

async function disconnect() {
	return await client.close();
}

async function monitorCollection(
	collectionName: string,
	onChange: (doc: Document) => void,
	pipeline: Document[] = []
) {

	const collection = db.collection(collectionName);

	const changeStream = collection.watch(pipeline);


	changeStream.on("change", (next: Document) => {
		//console.log("onChangeDocument", next);
		onChange(next["fullDocument"] as Document);
	});
}

async function getUserByEmail(userEmail: string): Promise<User | undefined> {

	const user: User = await db.collection("users").findOne({
		email: { $eq: userEmail },
		active: { $eq: true }
	}) as unknown as User;

	if (!user) {
		console.warn("User email not found in DB: ", userEmail);
		return undefined;
	}

	return user;
}

async function getUserById(userId: string): Promise<User | undefined> {

	const user: User = await db.collection("users").findOne({
		_id: { $eq: userId as unknown as ObjectId },
	}) as unknown as User;

	if (!user) {
		console.warn("User ID not found in DB: ", userId);
		return undefined;
	}

	return user;
}


// eslint-disable-next-line @typescript-eslint/no-explicit-any
async function getTasks(sendData: (doc: any) => void) {

	const cursor = db.collection("tasks").find({
		active: { $eq: true }
	}, {
		projection: { _id: 0 },
	});

	while (await cursor.hasNext()) {
		sendData(await cursor.next());
	}
}


// eslint-disable-next-line @typescript-eslint/no-explicit-any
async function getTasksByUser(user: User, sendData: (doc: any) => void, monitor: boolean = true) {

	const groupIds = user.groups || [];

	groupIds.push(user.id);

	const groupsCursor = db.collection("groups").find({
		participants: { $in: [ user.email ] },
	});

	await groupsCursor.forEach((doc) => {
		groupIds.push(doc.id);
	});

	const cursor = db.collection("tasks").find({
		group: { $in: groupIds },
		// active: { $eq: true } // Needed all to delete tasks on client side
	}, {
		projection: { _id: 0 },
	});

	while (await cursor.hasNext()) {
		sendData(await cursor.next());
	}

	if (monitor) {
		monitorCollection(
			"tasks",
			sendData,
			[
				{
					"$match": {
						"fullDocument.group": { $in: groupIds },
					}
				}
			]
		);
	}
}


async function saveDoc({
	type,
	doc,
}: {
  type: string,
  doc: Task | Group | User | Done | Token | Score,
}): Promise<void> {

	if (!doc) {
		console.log("Doc is: ", doc);
		return;
	}

	const oldDoc = await db.collection(type).findOne(
		{
			_id: { $eq: doc.id as unknown as ObjectId },
		}
	);

	// console.log("Old doc: ", oldDoc);

	if (!oldDoc) {

		logger.info(`Inserting new doc: ${JSON.stringify(doc)}`);

		try {
			await db.collection(type).insertOne({
				_id: doc.id as unknown as ObjectId,
				...doc,
			});
		} catch (e) {
			console.error("insert task error", e);
		}

	} else {

		const oldDocLastDateModified = new Date(oldDoc.lastDateModified);
		// console.log("old doc date ", oldDocLastDateModified);
		const newDocLastDateModified = new Date(doc.lastDateModified);
		// console.log("new doc date ", newDocLastDateModified);

		if (!oldDoc.lastDateModified || (newDocLastDateModified > oldDocLastDateModified)) {

			/* const newDoc =  */await db.collection(type).replaceOne(
				{
					_id: { $eq: doc.id as unknown as ObjectId },
				},
				doc
			);

			// console.log("New doc: ", newDoc);
		}
	}
}

async function getTaskById(taskId: string): Promise<Task | undefined> {

	const task: Task = await db.collection("tasks").findOne({
		_id: { $eq: taskId as unknown as ObjectId },
	}) as unknown as Task;

	if (!task) {
		console.warn("Task ID not found in DB: ", taskId);
		return undefined;
	}

	return task;
}


async function getGroupById(groupId: string): Promise<Group | undefined> {

	const group: Group = await db.collection("groups").findOne({
		_id: { $eq: groupId as unknown as ObjectId },
	}) as unknown as Group;

	if (!group) {
		console.warn("Group ID not found in DB: ", groupId);
		return undefined;
	}

	return group;
}


// TODO: Send groups user was excluded from participants
async function getGroups(user: User, sendData?: (doc: Document) => void, monitor: boolean = true) {

	const cursor = db.collection("groups").find({
		$or: [
			{
				participants: user.email
			},
			{
				invited: user.email
			}
		]
	});

	while (await cursor.hasNext()) {
		const group = await cursor.next();
		console.log("getGroups", group);
		if (group) {
			sendData?.(group);
		}
	}

	const personalGroup: Group = {
		type: "group",
		name: "personal",
		id: user.id,
		active: true,
		author: user.email,
		participants: [ user.email ],
		lastDateModified: new Date().toISOString(),
	};

	sendData?.(personalGroup);

	if (monitor) {
		if (sendData) {
			monitorCollection("groups", sendData, [
				{
					"$match": {
						$or: [
							{
								"fullDocument.participants": { $in: [ user.email ] },
							},
							{
								"fullDocument.invited": { $in: [ user.email ] },
							}
						]
					}
				}
			]);
		}
	}
}


// eslint-disable-next-line @typescript-eslint/no-explicit-any
/* async function getInvitedGroups(user: User, sendData?: (doc: any) => void) {

	const cursor = db.collection("groups").find({
		invited: { $all: [ user.email ] }
	});

	while (await cursor.hasNext()) {
		const group: Group | null = await cursor.next() as unknown as Group;
		logger.info("getInvitedGroups", group);
		if (group) {
			sendData?.(group);
		}
	}

	if (sendData) {
		monitorCollection("groups", sendData, [
			{
				"$match": {
					"fullDocument.invited": { $all: [ user.email ] },
				}
			}
		]);
	}
} */


// eslint-disable-next-line @typescript-eslint/no-explicit-any
async function getDones(user: User, sendData: (doc: any) => void) {
	console.log("Get dones for user: ", user);

	const groupIds = user.groups || [];

	groupIds.push(user.id);

	const groupsCursor = db.collection("groups").find({
		participants: { $in: [ user.email ] },
	});

	await groupsCursor.forEach((doc) => {
		groupIds.push(doc.id);
	});

	const cursor = db.collection("dones").find({
		group: { $in: groupIds },
		// active: { $eq: true }, // Needed to delete dones on client side
	});

	while (await cursor.hasNext()) {
		const done: Done | null = await cursor.next() as unknown as Done;
		if (done) {
			sendData(done);
		}
	}

	monitorCollection("dones", sendData, [
		{
			"$match": {
				"fullDocument.group": { $in: groupIds },
			}
		}
	]);
}

async function getDonesByTask(taskId: string): Promise<Done[]> {

	const cursor = db.collection<Done>("dones").find({
		task: { $eq: taskId }
	});

	return cursor.toArray() || [];
}


async function getScores(user: User, sendData: (doc: Document) => void) {
	console.log("Get scores for user: ", user);

	const groupIds = user.groups || [];

	groupIds.push(user.id);

	const groupsCursor = db.collection("groups").find({
		participants: { $in: [ user.email ] },
	});

	await groupsCursor.forEach((doc) => {
		groupIds.push(doc.id);
	});

	const cursor = db.collection("scores").find({
		groupId: { $in: groupIds },
		// active: { $eq: true }, // Needed to delete dones on client side
	});

	while (await cursor.hasNext()) {
		const score: Score | null = await cursor.next() as unknown as Score;
		if (score) {
			sendData(score);
		}
	}

	monitorCollection("scores", sendData, [
		{
			"$match": {
				"fullDocument.groupId": { $in: groupIds },
			}
		}
	]);
}


async function getTokensByEmails(emails: string[]): Promise<Token[]> {

	const cursor = db.collection<Token>("tokens").find({
		email: { $all: emails }
	});

	return cursor.toArray() || [];
}


export default {
	connect,
	disconnect,
	monitorCollection,
	getTasks,
	getTasksByUser,
	saveDoc,
	getGroups,
	getGroupById,
	getUserByEmail,
	getUserById,
	getDones,
	getTokensByEmails,
	getDonesByTask,
	getTaskById,
	getScores,
};
