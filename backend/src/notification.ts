import { fireConfig } from "../FirebaseConfig.js";
import { google } from "googleapis";
import dbConnection from "./dbConnection.js";
import { Task } from "./entities/Task.js";
import { User } from "./entities/User.js";
// import { Group } from "./entities/Group.js";
import logger from "./logger.js";
import { messaging } from "./main.js";
import { Document } from "mongodb";


const MESSAGING_SCOPE = "https://www.googleapis.com/auth/firebase.messaging";
const SCOPES = [ MESSAGING_SCOPE ];


async function sendMessageToTopic(task: Task) {
	logger.info(`Sending message for task ${JSON.stringify(task)}`);

	if (!task.group) {
		logger.info(`No group id: ${JSON.stringify(task)}`);
		return;
	}

	const message = {
		topic: task.group,
		data: {
			taskId: task.id || "",
			title: "Reminder",
			body: "It's time to " + task.name,
			link: `https://grouptasks.app/dashboard/${task.id}`,
		},
		/* notification: {
			title: "Reminder",
			body: "It's time to " + task.name,
		},
		webpush: {
			fcm_options: {
				link: `https://grouptasks.app/dashboard/${task.id}`
			},
			headers: {
				Urgency: "high"
			},
			notification: {
				body: "It's time to " + task.name,
				requireInteraction: true,
			}
		} */
	};

	messaging.send(message)
		.then((response) => {
			console.log(`Response for task ${task.name} message: `, response);
		})
		.catch((error) => {
			console.log(`Error for sending message for task: ${task.name} is: `, error);
		});

}


async function sendNotification(
	task: Task,
	registrationTokens: string[],
) {

	logger.info(`Sending notification: ${task.id}`);

	if (!task.group) return;

	let accessToken: string = "";

	try {
		accessToken = await getAccessToken() as string;
	} catch (error) {
		console.warn("Could not get access token ", error);
	}

	logger.info(accessToken);

	if (!accessToken) return;

	let notificationKey: string | undefined;

	notificationKey = await getDevicesGroupKey(task.group, accessToken);

	if (!notificationKey) {
		notificationKey = await createDevicesGroup(task.group, registrationTokens, accessToken);
	}

	logger.info(`notification_key: ${notificationKey}`);

	const message = {
		token: notificationKey,
		data: {
			taskId: task.id || "",
			title: "Reminder",
			body: "It's time to " + task.name,
			link: `https://grouptasks.app/dashboard/${task.id}`,
		},
		notification: {
			title: "Reminder",
			body: "It's time to " + task.name,
		},
		webpush: {
			fcm_options: {
				link: `https://grouptasks.app/dashboard/${task.id}`
			},
			headers: {
				Urgency: "high"
			},
			notification: {
				body: "It's time to " + task.name,
				requireInteraction: true,
			}
		}
	};

	if (notificationKey) postNotifiaction(message, accessToken);
}


async function postNotifiaction(message: object, accessToken: string) {

	logger.info(`Posting notification ${JSON.stringify(message)}`);

	const messageDto = {
		"message": message,
	};

	if (!accessToken) return;

	const response = await fetch("https://fcm.googleapis.com/v1/projects/balancer-4b310/messages:send", {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
			Authorization: `Bearer ${accessToken}`,
		},
		// referrerPolicy: "no-referrer",
		body: JSON.stringify(messageDto),
	});

	console.log("Notification response: ", response);
}


async function createDevicesGroup(
	groupId: string,
	registrationTokens: string[],
	accessToken: string
): Promise<string | undefined> {

	logger.info(`Creating devices group: ${registrationTokens}`);

	let notification;

	const message = {
		"operation": "create",
		"notification_key_name": groupId,
		"registration_ids": registrationTokens,
	};

	const response: Response = await fetch("https://fcm.googleapis.com/fcm/notification", {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
			access_token_auth: "true",
			Authorization: `Bearer ${accessToken}`,
			project_id: fireConfig.messagingSenderId,
		},
		// referrerPolicy: "no-referrer",
		body: JSON.stringify(message),
	});

	logger.info(`Create device group response: ${response}`);

	try {
		notification = await response?.json();
		logger.info("Notification: ", notification);
	} catch (error) {
		logger.warn("Devices group not created: ", error);
	}

	const notificationKey = notification?.notification_key;
	
	return notificationKey;
}


async function getDevicesGroupKey(groupId: string, accessToken: string): Promise<string> {
	let notification;

	const response: Response = await fetch(`https://fcm.googleapis.com/fcm/notification?notification_key_name=${groupId}`, {
		method: "GET",
		headers: {
			"Content-Type": "application/json",
			access_token_auth: "true",
			Authorization: `Bearer ${accessToken}`,
			project_id: fireConfig.messagingSenderId,
		},
	});

	try {
		notification = await response?.json();
		logger.info("Notification: ", notification);
	} catch (error) {
		console.warn("Devices group not found: ", error);
		return "";
	}

	const notificationKey = notification?.notification_key;

	return notificationKey;
}


function getAccessToken() {
	return new Promise(function(resolve, reject) {
		const key = fireConfig;
		const jwtClient = new google.auth.JWT(
			key.client_email,
			undefined,
			key.private_key,
			SCOPES,
			undefined
		);
		jwtClient.authorize(function(err, tokens) {
			if (err) {
				reject(err);
				return;
			}
			resolve(tokens?.access_token);
		});
	});
}


async function addTokenToDevicesGroups(token: string, user: User): Promise<void> {
	logger.info(`Adding token to devices group: ${token}`);

	let accessToken: string = "";

	try {
		accessToken = await getAccessToken() as string;
	} catch (error) {
		logger.warn(`Could not get access token: ${error}`);
	}

	logger.info(`Access token: ${accessToken}`);

	if (!accessToken) return;

	async function addDeviceTokenToDevicesGroup(group: Document) {

		let notificationKey: string | undefined;

		notificationKey = await getDevicesGroupKey(group.id, accessToken);

		logger.info(`notification_key: ${notificationKey}`);

		if (!notificationKey) {

			notificationKey = await createDevicesGroup(group.id, [ token ], accessToken);

		} else {

			const message = {
				"operation": "add",
				"notification_key_name": group.id,
				"notification_key": notificationKey,
				"registration_ids": [ token ],
			};

			fetch("https://fcm.googleapis.com/fcm/notification", {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					access_token_auth: "true",
					Authorization: `Bearer ${accessToken}`,
					project_id: fireConfig.messagingSenderId,
				},
				body: JSON.stringify(message),
			}).then((response: Response) => {
				console.log("Add device token response: ", response);
			});
			
		}
		
	}

	dbConnection.getGroups(user, addDeviceTokenToDevicesGroup, false);
}

async function subscribeToTopic(token: string, user: User) {
	logger.info(`Subscribing user to topics: ${token}`);

	async function addDeviceTokenToTopics(group: Document) {

		messaging.subscribeToTopic(token, group.id)
			.then((response) => {
				console.log(`Response for topic ${group.id} subscribe is: `, response);
			})
			.catch((error) => {
				console.error(`Error subscribing to topic: ${group.id}`, error);
			});
		
	}

	dbConnection.getGroups(user, addDeviceTokenToTopics, false);
}


export default {
	sendNotification,
	addTokenToDevicesGroups,
	subscribeToTopic,
	sendMessageToTopic,
};