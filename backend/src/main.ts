import { WebSocketServer, WebSocket } from "ws";
import dbConnection from "./dbConnection.js";
import { Document } from "mongodb";
import { v4 as uuidv4 } from "uuid";
import authenticate from "./authentication.js";
import reminders from "./reminders.js";
import { Group, Task, User, Message, Invitation, Done, Reminder, Score, Token } from "./entities";
import notification from "./notification.js";
import logger from "./logger.js";
import { initializeApp } from 'firebase-admin/app';
import admin from 'firebase-admin';
import { getMessaging } from "firebase-admin/messaging";
import { getAuth } from "firebase-admin/auth";
import { fireConfig } from "../FirebaseConfig.js";


interface CustomWebSocket extends WebSocket {
  id: string;
}

logger.info("Created server");
const wss = new WebSocketServer({ port: 8080, /* path: "/socket" */ });
logger.info("Created websocket");
const remindersMap: Reminder[] = [];
const firebase = await initializeApp({
	credential: admin.credential.cert(fireConfig)
});
export const messaging = getMessaging(firebase);
export const auth = getAuth(firebase);


await dbConnection.connect();

async function startServer() {

	const socketsMap: Map<string, CustomWebSocket[]> = new Map();

	wss.on("connection", function connection(ws: CustomWebSocket) {
		// console.log("New connection: ", ws);

		let userId: string | undefined;

		ws.id = uuidv4();
		console.log("Socket opened: ", ws.id);

		ws.on("error", (event: Event) => {
			console.error(event);
			if (userId) socketsMap.delete(userId);
			ws.close();
		});

		ws.on("message", async (event: MessageEvent) => {
			// logger.info(`Received message ${event}`);
			//logger.info(`userId: ${userId}`);

			let message: Message | undefined;
    
			try {
				message = JSON.parse(event as unknown as string);
			// console.log("data: ", data);
			} catch (e) {
				console.error(e);
			}

			if (!message) {
				return;
			}
    
			if (!userId) {
				if (message.type === "auth") {
					const user: User = message.data as User;
					userId = await authenticate.authenticateClient(user.accessToken, ws);
					dbConnection.saveDoc({
						type: "users",
						doc: user
					});
					sendMessage({ type: "authenticated" } as unknown as Document);
					return;
				} else {
					console.log("Unauthenticated", message.type);
					return;
				}
			}

			if (!userId) {
				return;
			}

			const user = await dbConnection.getUserById(userId);

			// console.log("Message from user: ", user);

			if (!user) {
				console.error("verifyIdToken", "User not found: ", userId);
				return;
			}

			const socketsList: CustomWebSocket[] = socketsMap.get(user.id) || [];

			socketsList.push(ws);
			socketsMap.set(user.id, socketsList);

			switch (message.type) {
			case "getTasks":
				console.log("getTasks: ", user);
				dbConnection.getTasksByUser(user, sendMessage);
				break;
			case "saveTask": {
				// console.log("Save task: ", message.data);
				const task: Task = message.data as Task;
				dbConnection.saveDoc({
					type: "tasks",
					doc: task,
				}).then(() => {
					if (task.active) {
						reminders.checkReminder(task, remindersMap);
					}
				});
			}
				break;
			case "saveGroup": {
			// don't save personal group
				const group: Group = message.data as Group;
				if (group.id === user?.id) {
					break;
				}
				dbConnection.saveDoc({
					type: "groups",
					doc:group,
				});
				break;
			}
			case "getGroups":
				dbConnection.getGroups(user, sendMessage);
				break;
			case "getDones":
				dbConnection.getDones(user, sendMessage);
				break;
			case "saveDone": {
				//logger.info("save Done");
				const done: Done = message.data as Done;
				dbConnection.saveDoc({
					type: "dones",
					doc: done,
				});
			}
				break;
			case "getScores":
				logger.info("get Scores");
				dbConnection.getScores(user, sendMessage);
				break;
			case "saveScore": {
				// logger.info("save Done");
				const score: Score = message.data as unknown as Score;
				dbConnection.saveDoc({
					type: "scores",
					doc: score,
				});
			}
				break;
			case "saveUser": {
				//console.log("save user", message.data);
				const user: User = message.data as User;
				dbConnection.saveDoc({
					type: "users",
					doc: user
				});
			}
				break;
			case "saveToken": {
				console.log("save token");
				const token: Token = message.data as Token;
				console.log("saveToken", message.data);
				dbConnection.saveDoc({
					type: "tokens",
					doc: token
				});
				notification.subscribeToTopic(token.id, user);
				break;
			}
			case "inviteUser": {
				logger.info("invite user");
				const invitation: Invitation = message.data as Invitation;
				const group: Group | undefined = await dbConnection.getGroupById(invitation.groupId);
				if (!group) break;
				if (group.participants.includes(invitation.email)) {
					logger.warn(`User ${invitation.email} exists in participants of group ${group.name}`);
					break;
				}
				if (group.invited?.length && group.invited?.length >= 0) {
					if (!group.invited.includes(invitation.email)) {
						group.invited?.push(invitation.email);
					}
				} else {
					group.invited = [ invitation.email ];
				}
				group.lastDateModified = new Date().toISOString();
				dbConnection.saveDoc({ type: "groups", doc: group });
			}
				break;
			default:
				break;
			}

		});

		function sendMessage(doc: Document) {
			delete doc._id;
			// console.log("Sending message ", doc);
			ws.send(JSON.stringify(doc));
		}

		ws.on("close", (event: Event) => {
			console.log("Closed ", event, ws.id);
			if (userId) socketsMap.delete(userId);
		});
	});

}

startServer();

reminders.loadReminders(dbConnection, remindersMap);

