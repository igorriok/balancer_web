import { Task } from "./entities/Task";
import getDateString from "./getDateString.js";
import dbConnection from "./dbConnection.js";
//import { Group } from "./entities/Group";
//import { Token } from "./entities/Token";
import notification from "./notification.js";
import logger from "./logger.js";
import { Reminder } from "./entities/Reminder";
import { Job, scheduleJob } from "node-schedule";


async function checkReminder(task: Task, remindersList: Reminder[]) {
	// logger.info("Check remider for task: ", task);

	if (!task.active) {
		logger.info(`Task ${task.name} is inactive`);
		return;
	}

	/* const dbTask: Task | undefined = await dbConnection.getTaskById(task.id);
	if (dbTask?.reminderDate === task.reminderDate && dbTask?.reminderTime === task.reminderTime) {
		logger.info(`Task ${task.name} has old reminder`);
		return;
	} */

	// remove old reminder
	deleteReminder(task.id, remindersList);
	
	const reminderDateString: string = await getDateString(task);
	logger.info(`Reminder date string for task: ${task.name} - ${reminderDateString}`);

	if (!reminderDateString) {
		logger.info(`No reminder date for task: ${task.name}`);
		return;
	}

	const reminderDate: Date = new Date(reminderDateString);
	const currentDate: Date = new Date();

	if (currentDate > reminderDate) {
		logger.info(`Remider date for task: ${task.name} is in the past`);
		return;
	}
	
	const delay = reminderDate.getTime() - currentDate.getTime();
	logger.info(`Delay for task ${task.name} is: ${delay}`);

	//const timeOut = setTimeout(() => createMessage(task, remindersList), delay);

	const job: Job = scheduleJob(reminderDate, () => createMessage(task, remindersList));

	remindersList.push({
		taskId: task.id,
		job
	});

	// console.log("Remiders list: ", remindersList);
}


function deleteReminder(taskId: string, remindersList: Reminder[]) {
	remindersList.forEach((reminder: Reminder, index) => {
		if (reminder.taskId === taskId) {
			try {
				reminder.job.cancel();
				const removed = remindersList.splice(index, 1);
				logger.info(`Removed old remider for task ${taskId}: ${JSON.stringify(removed)}`);
			} catch (error) {
				logger.info(`Could not cancel timeout for task: ${taskId}`, error);
			}
		}
	});
}

async function createMessage(task: Task, remindersList: Reminder[]) {
	logger.info(`Creating message for task: ${task.name}`);

	// remove old reminder
	deleteReminder(task.id, remindersList);

	notification.sendMessageToTopic(task);
}


/* async function createNotification(task: Task, remindersList: Reminder[]) {
	logger.info(`Creating notification for task: ${task.name}`);

	// remove old reminder
	deleteReminder(task.id, remindersList);

	if (!task.active) {
		logger.info(`Task ${task.name} is no more active`);
		return;
	}

	const groupId = task.group;

	if (!groupId) {
		console.warn(`Task ${task.name} without group ID`);
		return;
	}

	let emails: string[] = [];

	if (task.groupName === "personal" && task.author) {
		emails.push(task.author);
	} else {
		const group: Group | undefined = await dbConnection.getGroupById(groupId);

		emails = group?.participants || [];
	}

	// logger.info("Emails", emails);

	if (emails.length === 0) {
		logger.info(`Emails list is empty ${task}`);
		return;
	}

	const tokens: Token[] = await dbConnection.getTokensByEmails(emails);

	logger.info(`Tokens for emails: ${emails} are ${JSON.stringify(tokens)}`);

	const tokensIds = tokens.map((token: Token) => token.id);

	notification.sendNotification(task, tokensIds);

	logger.info(`Reminders list size: ${remindersList.length}`);
	remindersList.forEach((reminder, index) => {
		logger.info(`Reminder for taskId: ${reminder.taskId} is ${index}`);
	});
} */

// TODO: Move to service worker
async function loadReminders(db: typeof dbConnection, remindersMap: Reminder[]) {
	logger.info("Loading reminders");

	db.getTasks((task) => checkReminder(task, remindersMap));
}

export default {
	checkReminder,
	loadReminders,
};
