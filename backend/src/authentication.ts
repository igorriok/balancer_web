import { DecodedIdToken } from "firebase-admin/auth";
import { WebSocket } from "ws";
import { auth } from "./main.js";
import logger from "./logger.js";

const socketsMap: Map<string, WebSocket[]> = new Map();


async function authenticateClient(token: string, ws: WebSocket): Promise<string | undefined> {

	console.log("authenticateClient", "Checking token: ", token);

	if (!token) {
		logger.info("No token");
		return;
	}
    
	let decodedToken: DecodedIdToken;
  
	try {
		decodedToken = await auth.verifyIdToken(token);
	} catch (error) {
		console.error("authenticateClient", error);
		return undefined;
	}
  
	const uId = decodedToken?.uid;
  
	if (uId) {
  
		const socketsList: WebSocket[] = socketsMap.get(decodedToken?.uid) || [];
  
		socketsList.push(ws);
  
		socketsMap.set(decodedToken?.uid, socketsList);
	}
  
	return uId;
}

export default {
	authenticateClient,
};