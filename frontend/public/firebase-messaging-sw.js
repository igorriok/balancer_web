importScripts("https://www.gstatic.com/firebasejs/8.10.1/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/8.10.1/firebase-messaging.js");

importScripts("FirebaseConfig.js");

firebase.initializeApp(fireConfig);
const messaging = firebase.messaging();

messaging.onBackgroundMessage((payload) => {
	console.dir(payload);

	const notificationTitle = payload.data?.title;
	const notificationOptions = {
		body: payload.data?.body,
		data: {
			link: payload.data?.taskId,
		},
		tag: payload.data?.taskId,
	};

	self.registration.showNotification(notificationTitle, notificationOptions);
});

/* messaging.onMessage((payload) => {
	console.log("Message received. ", payload);

	const notificationTitle = payload.data?.title;
	
	const notificationOptions = {
		body: payload.data?.body,
		data: {
			link: payload.data?.taskId,
		},
		tag: payload.data?.taskId,
	};

	self.registration.showNotification(notificationTitle, notificationOptions);
}); */

self.addEventListener("notificationclick", (event) => {
	console.log(event);
	event.notification.close();
	const taskId = event.notification.data.link;
	if (clients.openWindow) clients.openWindow("/dashboard/" + taskId);
});
