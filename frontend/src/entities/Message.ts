import { Done } from "./Done";
import { Group } from "./Group";
import { Invitation } from "./Invitation";
import { MessageToken } from "./MessageToken";
import { Score } from "./Score";
import { Task } from "./Task";
import { User } from "./User";

export type Message = {
    type: string,
    data?: Task | Done | User | Group | MessageToken | Invitation | Score,
}
