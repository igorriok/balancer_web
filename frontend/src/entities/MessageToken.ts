export type MessageToken = {
    id: string,
    lastDateModified: Date,
    email: string,
};
