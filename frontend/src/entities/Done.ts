
export interface Done {
    type: string;
    id?: string;
    author: string;
    date?: string;
    group: string;
    groupName?: string;
    task?: string;
    taskName?: string;
    averageScore?: number;
    arbiters?: string[];
    active?: boolean;
    lastDateModified?: string;
}
