export interface User {
	email: string;
	displayName: string;
	photoURL: string;
	id: string;
	accessToken: string;
	roles: string[];
	groups?: string[];
}
