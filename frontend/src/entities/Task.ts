export type Task = {
	type: string;
	id?: string;
	name: string;
	description?: string;
	author?: string;
	addedDate?: string;
	group: string;
	groupName?: string;
	recurring?: boolean;
	active?: boolean;
	interval?: number;
	priority?: number;
	calculatePriority?: (task: Task, setTask?: (task: Task) => void) => Promise<void>;
	reminderDate?: string;
	reminderTime?: string;
	timeZone?: string;
	offset?: string; // Format should be +00:00
	reminderId?: string;
	lastDateModified?: string;
}
