export interface Group {
	type: string;
	id: string;
	name: string;
	author?: string;
	participants: string[];
	active: boolean;
	invited: string[];
	lastDateModified?: string;
}
