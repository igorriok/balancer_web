import { useLangContext } from "../context/LangContext";
import { texts } from "./text";

export default function useTranslate(word: string): string {

	const useLang: any = useLangContext();
	return texts.get(word)?.get(useLang?.lang) || word;
}
