
type SocketProps = {
	onMessage?: (event: MessageEvent) => void;
}

export default function useWebSocket(props: SocketProps) {

	const { onMessage } = props;

	const socket = new WebSocket("ws://localhost:8080");

	console.log(socket);

	// Connection opened
	socket.addEventListener("open", (event) => {
		console.log("Socket opened", event);
		socket.send("Hello Server!");
	});

	// Listen for messages
	socket.addEventListener("message", (event: MessageEvent) => {
		console.log("Message from server ", event.data);
		onMessage && onMessage(event);
	});

	const closeSocket = () => socket.close();

	return { socket, closeSocket };
}
