import { createContext, useContext } from "react";


export interface BackendContextType {
	webSocket: WebSocket | undefined;
}

export const BackendContext = createContext<BackendContextType | null>(null);


export function useBackendContext(): BackendContextType | null {
	return useContext(BackendContext);
}
