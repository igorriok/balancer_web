import { Firestore } from "firebase/firestore";
import { createContext, useContext } from "react";


export interface DatabaseContextType {
	db: Firestore | undefined;
}

export const DatabaseContext = createContext<DatabaseContextType>(null!);


export function useDatabaseContext(): DatabaseContextType {
	return useContext(DatabaseContext);
}
