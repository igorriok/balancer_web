import * as functions from "firebase-functions";
import { HttpsError } from "firebase-functions/v2/https";
import { onTaskDispatched } from "firebase-functions/v2/tasks";
import { initializeApp } from "firebase-admin/app";
import { fireConfig } from "../FirebaseConfig";
import { firestore } from "firebase-admin";
import { MulticastMessage, getMessaging } from "firebase-admin/messaging";
import { FieldValue, Timestamp, getFirestore } from "firebase-admin/firestore";
import { Task } from "./Task";
const admin = initializeApp(fireConfig);
const db = getFirestore(admin);
import { log, warn, error } from "firebase-functions/logger";
import { CloudTasksClient } from "@google-cloud/tasks";
import { google } from "@google-cloud/tasks/build/protos/protos";
import { Done } from "./Done";
import getDateString from "./getDateString";
import getDaysDifference from "./getDaysDifference";
import getReminderDate from "./getDate";

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });

const queueName = "reminder";
const location = "us-central1";


export const inviteUser = functions.https.onCall((data, context) => {

	functions.logger.info("Invite user", { structuredData: true });

	const authorEmail = context.auth?.token.email;

	functions.logger.info("Token email " + authorEmail, { structuredData: true });

	const invitedUserEmail = data.invited;
	const groupId = data.group;

	const groupRef = db.doc("groups/" + groupId);

	groupRef.update({
		invited: FieldValue.arrayUnion(invitedUserEmail),
	});

	const invitedUserRef = db.doc("users/" + invitedUserEmail);

	invitedUserRef.get().then((doc) => {

		if (doc.exists) {
			invitedUserRef.update({
				invited: FieldValue.arrayUnion(groupId),
			});
		} else {
			invitedUserRef.set({
				invited: [groupId],
			});
		}

	});

});


export const getUserNicknames = functions.https.onCall(
	async (data, context): Promise<Record<string, string>> => {

		functions.logger.info(
			"Get nicknames for: " + data.emails,
			{ structuredData: true },
		);

		const authorEmail = context.auth?.token.email;

		functions.logger.info(
			"Token email " + authorEmail,
			{ structuredData: true },
		);

		const userEmails: string[] = data.emails;

		const usersNicknames: Record<string, string> = {};

		for (const email of userEmails) {

			const userRef = db.doc("users/" + email);

			const nickname = await userRef.get().then((doc) => {
				if (doc.exists) {
					return doc.data()?.nickname;
				}
				return "deleted account";
			});

			usersNicknames[email] = nickname;
		}

		console.log("Got nicknames: ", usersNicknames);
		
		return usersNicknames;
	},
);


export const acceptInvitation = functions.https.onCall((data, context) => {

	functions.logger.info("Accept invitation", { structuredData: true });

	const userEmail = context.auth?.token.email;

	functions.logger.info("Token email " + userEmail, { structuredData: true });

	const groupId = data.group;

	const groupRef = db.doc("groups/" + groupId);

	groupRef.update({
		invited: FieldValue.arrayRemove(userEmail),
	});

	groupRef.update({
		participants: FieldValue.arrayUnion(userEmail),
	});

	const invitedUserRef = db.doc("users/" + userEmail);

	invitedUserRef.get().then((doc) => {

		if (doc.exists) {

			invitedUserRef.update({
				invited: FieldValue.arrayRemove(groupId),
			});

			invitedUserRef.update({
				groups: FieldValue.arrayUnion(groupId),
			});

		}

	});

});


export const refuseInvitation = functions.https.onCall((data, context) => {

	functions.logger.info("Refuse invitation", { structuredData: true });

	const userEmail = context.auth?.token.email;

	functions.logger.info("Token email " + userEmail, { structuredData: true });

	const groupId = data.group;

	const groupRef = db.doc("groups/" + groupId);

	groupRef.update({
		invited: FieldValue.arrayRemove(userEmail),
	});

	const invitedUserRef = db.doc("users/" + userEmail);

	invitedUserRef.get().then((doc) => {

		if (doc.exists) {

			invitedUserRef.update({
				invited: FieldValue.arrayRemove(groupId),
			});

		}

	});

});


export const deleteDoneTask = functions.https.onCall(
	async (data, context): Promise<string> => {

		functions.logger.info("Delete done " + data.id, { structuredData: true });

		const userEmail = context.auth?.token.email;

		const doneRef = db.doc("dones/" + data.id);

		// db.recursiveDelete(doneRef);

		const doc = await doneRef.get().then((doc) => {
			return doc;
		});

		let result = "failed";

		if (doc.exists) {
			if (doc.data()?.author == userEmail) {
				const response = doneRef.delete();
				const resp = await response.then((res: firestore.WriteResult) => {
					functions.logger.info(
						res.writeTime,
						{ structuredData: true },
					);
					return res.writeTime ? "deleted" : "not deleted";
				});

				return resp;
			} else {
				functions.logger.info(
					"This done" + data.id + "don't belong to this user " + userEmail,
					{ structuredData: true },
				);
				result = "This done "+data.id+" don't belong to this user "+userEmail;
			}
		} else {
			functions.logger.info(
				"Can't find done " + data.id,
				{ structuredData: true },
			);
			result = "Can't find done " + data.id;
		}

		return result;
	});


export const scoreDoneTask = functions.https.onCall(
	async (data, context): Promise<string> => {

		functions.logger.info("Score done " + data.doneId,
			{ structuredData: true },
		);

		const userEmail = context.auth?.token.email;

		const doneRef = db.doc("dones/" + data.doneId);

		const doc = await doneRef.get().then((doc) => {
			return doc;
		});

		let result = "failed";

		if (doc.exists) {

			const scoresRef = db.collection("dones/" + data.doneId + "/scores");

			scoresRef.add({
				email: userEmail,
				points: data.points,
				date: Timestamp.fromDate(new Date(Date.now())),
			});

			doneRef.update({
				arbiters: FieldValue.arrayRemove(userEmail),
			});

			result = "succes";

		} else {
			functions.logger.info(
				"Can't find done " + data.doneId,
				{ structuredData: true },
			);
			result = "Can't find done " + data.doneId;
		}

		return result;
	});
	

export const deleteScoreRequest = functions.https.onCall(
	async (data, context): Promise<string> => {

		functions.logger.info(
			"Delete score request for Done: " + data.id, { structuredData: true },
		);

		const userEmail = context.auth?.token.email;

		const doneRef = db.doc("dones/" + data.id);

		const doc = await doneRef.get().then((doc) => {
			return doc;
		});

		let result = "failed";

		if (doc.exists) {

			doneRef.update({
				arbiters: FieldValue.arrayRemove(userEmail),
			});

			result = "succes";

		} else {
			functions.logger.info(
				"Can't find done " + data.id,
				{ structuredData: true },
			);
			result = "Can't find done " + data.id;
		}

		return result;
	});


// eslint-disable-next-line @typescript-eslint/no-unused-vars, require-jsdoc
function sendNotification(
	task: Task,
	registrationTokens: string[],
) {

	const message: MulticastMessage = {
		tokens: registrationTokens,
		/* notification: {
			title: "Reminder",
			body: "It's time for " + task.name,
			imageUrl: "https://grouptasks/dashboard/" + task.id,
		}, */
		data: {
			taskId: task.id || "",
			title: "Reminder",
			body: "It's time to " + task.name,
		},
		fcmOptions: {
			analyticsLabel: task.id,
		},
	};

	getMessaging(admin).sendEachForMulticast(message)
		.then((response) => {
			if (response.failureCount > 0) {
				const failedTokens: string[] = [];
				response.responses.forEach((resp, idx) => {
					if (!resp.success) {
						failedTokens.push(registrationTokens[idx]);
					}
				});

				log(
					"List of tokens that caused failures",
					{ failedTokens: failedTokens },
				);
			}
		});
	
}


export const updateDone = functions.firestore
	.document("dones/{doneId}")
	.onWrite( async (change) => {
		log(
			"Done updated", { done: change.after.data() },
		);

		const done: Done = change.after.data() as Done;

		if (!done) {
			log("Done not found", { done: change.before.data() });
			return 0;
		}

		const doneDate: Date = done.date.toDate();

		log("Done date: ", { date: doneDate });

		// Update task with new reminder
		
		const taskRef = db.doc("tasks/" + done.task);
		const dbTaskObj = await taskRef.get();

		if (!dbTaskObj.exists || !dbTaskObj.data()) {
			log("Task is missing in DB", { task: done.task });
			return 0;
		}
	
		const task: Task = dbTaskObj.data() as Task;

		if (!task.recurring) {
			log("Task not recurring");
			return 0;
		}

		let interval: number;

		try {
			interval = Number(task.interval);
		} catch (err) {
			error("Can't convert interval", { interval: task.interval, error: err });
			interval = 0;
		}

		const reminderDateTime: Date = new Date(doneDate);

		log("Done date: ", { doneDate: doneDate.getDate() });

		reminderDateTime.setDate(doneDate.getDate() + interval);

		log("Reminder date: ", { reminderDateTime: reminderDateTime });

		const reminderDate = reminderDateTime.toISOString().split("T")[0];

		log("Reminder date: ", { reminderDate: reminderDate });

		taskRef.update({
			reminderDate: reminderDate,
		});

		return 0;
	});


export const updateTask = functions.firestore
	.document("tasks/{taskId}")
	.onWrite( async (change) => {
		log(
			"Task updated", { task: change.after.data() },
		);

		const task: Task = change.after.data() as Task;
		const oldTask: Task = change.before.data() as Task;

		if (!task.active) {
			return 0;
		}

		// Create a reminder in Google tasks
		if (task.reminderDate || task.reminderTime) {
			if ((task.reminderDate !== oldTask?.reminderDate || task.reminderTime !== oldTask?.reminderTime) ||
				(!task.reminderId && task.recurring)
			) {
			
				const response = await enqueueReminder(task);

				if (!response) {
					return 0;
				}

				log(
					"Created google task",
					{ name: response.name },
				);

				change.after.ref.set({
					reminderId: response.name,
				}, { merge: true });
			}
		}

		return 0;
	});

export const reminder = onTaskDispatched({
	retryConfig: {
		maxAttempts: 2,
		minBackoffSeconds: 60,
	},
	rateLimits: {
		maxConcurrentDispatches: 1,
		maxDispatchesPerSecond: 5,
	},
}, async (req) => {
	log("Starting reminder task", { task: req.data.task });
			
	const task: Task = req.data.task;

	if (!task) {
		warn("Invalid payload. Must include task.");
		throw new HttpsError(
			"invalid-argument",
			"Invalid payload. Must include task.",
		);
	}

	// check if reminder date is the same
	const taskRef = db.doc("tasks/" + task.id);
	const dbTaskObj = await taskRef.get();
	if (!dbTaskObj.exists || !dbTaskObj.data()) {
		log("Task is missing in DB", { task: task });
		return;
	}

	const dbTask: Task = dbTaskObj.data() as Task;

	if (!dbTask.active) {
		log("Task is not active", { task: task });
		return;
	}

	await taskRef.update({
		reminderId: "",
	});

	if (
		!(task.reminderDate === dbTask.reminderDate) ||
		!(task.reminderTime === dbTask.reminderTime)
	) {
		log("Reminder date and time are different", { task: task });
		return;
	}

	const currentDateTime: Date = new Date();

	// Check if need to be rescheduled
	if (task.reminderDate && getDaysDifference(getReminderDate(dbTask), currentDateTime) >= 29) {

		currentDateTime.setDate(currentDateTime.getDate() + 29);

		log("Next date", { currentDateTime: currentDateTime });

		const gTask = createGTask(dbTask, currentDateTime);

		const client = new CloudTasksClient();
		const parent = client.queuePath(fireConfig.projectId!, location, queueName);
		const request = { parent: parent, task: gTask };

		await client.createTask(request);

		return;
	}

	const groupId = dbTask.group;

	const groupRef = db.doc("groups/" + groupId);

	const emails: string[] = await groupRef.get().then((doc) => {
		if (doc.exists) {
			return doc.data()?.participants;
		}
		return [];
	});

	if (dbTask.groupName === "Personal" && dbTask.author) {
		emails.push(dbTask.author);
	}

	log("Emails", { emails: emails });

	if (emails.length === 0) {
		log("Emails list is empty", { task: dbTask });
		return;
	}

	db.collectionGroup("tokens").where("email", "in", emails)
		.get().then((querySnapshot) => {
			log("Token list", { tokens_size: querySnapshot.size, tokens: querySnapshot.docs });
			querySnapshot.forEach((tokenDto) => {
				log("Token", { tokenDto: tokenDto });
				sendNotification(dbTask, [tokenDto.id]);
			});
		});
});

const createGTask = (task: Task, date: Date ) => {

	const payload = {
		data: {
			task: task,
		},
	};

	const serviceAccountEmail = fireConfig.serviceAccountId;
	
	const gTask = {
		httpRequest: {
			httpMethod: google.cloud.tasks.v2.HttpMethod.POST,
			url: "https://us-central1-balancer-4b310.cloudfunctions.net/reminder",
			body: Buffer.from(JSON.stringify(payload)).toString("base64"),
			headers: {
				"Content-Type": "application/json",
			},
			oidcToken: {
				serviceAccountEmail,
			},
		},
		scheduleTime: {
			seconds: date.getTime() / 1000,
		},
	};

	return gTask;
};


const enqueueReminder = async (task: Task): Promise<google.cloud.tasks.v2.ITask | null> => {
	log(
		"Enqueue task", { task: task },
	);

	const client = new CloudTasksClient();
	const parent = client.queuePath(fireConfig.projectId!, location, queueName);
	
	// Delete previous reminder
	if (task.reminderId) {
		log("Task has old reminder", { reminderId: task.reminderId });
		try {
			await client.deleteTask({ name: task.reminderId });
		} catch (error) {
			log("Reminder delete error", { error: error });
		}
	}

	const dateString = getDateString(task);
	log("Date string", { dateString: dateString });
	const currentDate = new Date(Date.now());
	log("Current date", { currentDate: currentDate });


	if (!dateString) {
		log("No date string", { dateString: dateString });
	}

	let remiderDate = new Date(dateString);
	log("Reminder date", { remiderDate: remiderDate });
	

	if (remiderDate < currentDate) {
		log(
			"Date is in the past",
			{ currentDate: currentDate, remiderDate: remiderDate },
		);
		return null;
	}

	const waitDays = getDaysDifference(currentDate, remiderDate);
	log("Wait days", { waitDays: waitDays });

	if (waitDays >= 29) {
		remiderDate = currentDate;
		remiderDate.setDate(currentDate.getDate() + 29);
	}
	log("Reminder date", { remiderDate: remiderDate });

	const gTask = createGTask(task, remiderDate);

	log("GTask", { gTask: gTask });

	const request = { parent: parent, task: gTask };
	const [response] = await client.createTask(request);

	return response;
};


