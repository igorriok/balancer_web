import { log } from "firebase-functions/logger";
import { Task } from "./Task";


/**
 * Extract date string from task
 * @param {Task} task task object from to get date string
 * @return {string} Date string
 */
export default function getDateString(task: Task): string {

	let dateString = "";
	const currentDateTime = new Date();
	const currentDate = currentDateTime.toISOString().split("T")[0];
	log("Current date", { currentDate: currentDate });
	const currentTime = currentDateTime.toISOString().split("T")[1]
		.substring(0, 8);
	log("Current time", { currentTime: currentTime });

	if (task.recurring) {

		if (task.reminderDate) {

			dateString = task.reminderDate;

			if (task.reminderTime) {
				// if we have time, add time
				dateString = dateString + "T" + task.reminderTime + task.offset;
			} else {
				// else add default time
				dateString = dateString + "T08:00:00" + task.offset;
			}

		} else if (task.reminderTime) {

			const taskDateTime = new Date(
				currentDate + "T" + task.reminderTime + task.offset,
			);

			log("Task dateTime", { taskDateTime: taskDateTime });

			if (taskDateTime < currentDateTime) {
				currentDateTime.setDate(currentDateTime.getDate() + 1);
				dateString = currentDateTime.toISOString().split("T")[0];
				dateString = dateString + "T" + task.reminderTime + task.offset;
			} else {
				dateString = taskDateTime.toISOString();
			}
		}
	} else {
		if (task.reminderDate) {
			dateString = task.reminderDate;

			if (task.reminderTime) {
				// if we have time, add time
				dateString = dateString + "T" + task.reminderTime + task.offset;
			} else {
				// else add default time
				dateString = dateString + "T08:00:00" + task.offset;
			}
		} else {
			if (task.reminderTime) {
				dateString = currentDateTime.toISOString().split("T")[0];
				dateString = dateString + "T" + task.reminderTime + task.offset;
			}
		}
	}

	return dateString;
}
