export interface Task {
	id?: string;
	name: string;
	description?: string;
	author?: string;
	addedDate?: Date;
	group?: string;
	groupName?: string;
	recurring?: boolean;
	active?: boolean;
	interval?: number;
	priority?: number;
	reminderDate?: string;
	reminderTime?: string;
	timeZone?: string;
	offset?: string; // Format should be +00:00
	reminderId?: string;
}
