import { Timestamp } from "firebase/firestore";

export interface Score {
    email: string;
    points: number;
}

export interface Done {
    id?: string;
    author: string;
    date: Timestamp;
    group: string;
    groupName?: string;
    task: string;
    taskName?: string;
    score?: Score[];
}
