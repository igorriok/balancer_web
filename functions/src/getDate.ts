import { Task } from "./Task";


/**
 * Extract date string from task
 * @param {Task} task task object from to get date string
 * @return {Date} Date
 */
export default function getReminderDate(task: Task): Date {

	const currentDateTime = new Date();
	const date = task.reminderDate ? task.reminderDate : currentDateTime.toISOString().split("T")[0];
	const time = task.reminderTime ? task.reminderTime : "T08:00:00";
	const offset = task.offset ? task.offset : "+00:00";

	const taskDateTime = new Date(
		date + "T" + time + offset,
	);
	
	return taskDateTime;
}
